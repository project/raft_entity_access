This module is based on Drupal core and patches for Drupal core which
are Copyright by the original authors.

Additional code for the contributed module including tests and
improvements necessary to make this a stand-alone module are
Copyright 2023 by Research Applications and Financial Tracking, Inc.
Further changes are Copyright by the original authors.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program as the file LICENSE.txt; if not, please see
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt.
