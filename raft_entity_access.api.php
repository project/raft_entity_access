<?php

/**
 * @file
 * Hooks specific to the RAFT Entity Access module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Inform the entity access system what permissions the user has.
 *
 * This hook is for implementation by entity access modules. In this hook,
 * the module grants a user different "grant IDs" within one or more
 * "realms". In hook_entity_access_records(), the realms and grant IDs are
 * associated with permission to view, edit, and delete individual entitys.
 *
 * The realms and grant IDs can be arbitrarily defined by your entity access
 * module; it is common to use role IDs as grant IDs, but that is not required.
 * Your module could instead maintain its own list of users, where each list has
 * an ID. In that case, the return value of this hook would be an array of the
 * list IDs that this user is a member of.
 *
 * A entity access module may implement as many realms as necessary to properly
 * define the access privileges for the entities. Note that the system makes no
 * distinction between published and unpublished entities. It is the module's
 * responsibility to provide appropriate realms to limit access to unpublished
 * content.
 *
 * In the default SQL implementation, the records are stored per entity type an
 * a table named {ENTITY_TYPE_entity_access} and define which grants are
 * required to access an entity. If there are no entity access modules
 * enabled (based on any implementing the generic or type-specific grants hooks
 * described below) then no modifications are made to the SQL query and access
 * checks fall back to allowed for 'view', allowed for 'view' and 'edit' for
 * those with the entity type's admin permission, and neutral in other cases.
 * There is a special case for a record with entity ID 0 corresponds to an
 * access all grant for the realm and grant ID of that record. Entity access
 * modules must create such a record for ID 0 for their custom realms in order
 * for the corresponding grant to work; for example, a module could create a
 * record in {ENTITY_TYPE_entity_access} with:
 *
 * @code
 * $record = [
 *   'id' => 0,
 *   'gid' => 888,
 *   'realm' => 'example_realm',
 *   'grant_view' => 1,
 *   'grant_update' => 0,
 *   'grant_delete' => 0,
 * ];
 * \Drupal::database()->insert('entity_access')->fields($record)->execute();
 * @endcode
 * And then in its hook_entity_grants() implementation, it would need to return:
 * @code
 * if ($op == 'view') {
 *   $grants['example_realm'] = [888];
 * }
 * @endcode
 * If you decide to do this, be aware that the entity_access_rebuild() function
 * will erase any entity ID 0 entry when it is called, so you will need to make
 * sure to restore your {entity_access} record after entity_access_rebuild() is
 * called.
 *
 * For a detailed example, see entity_access_example.module.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account object whose grants are requested.
 * @param string $op
 *   The entity operation to be performed, such as 'view', 'update', or 'delete'.
 *
 * @return array
 *   An array whose keys are "realms" of grants, and whose values are arrays of
 *   the grant IDs within this realm that this user is being granted.
 *
 * @see entity_access_rebuild()
 * @ingroup entity_access
 */
function hook_entity_grants(AccountInterface $account, string $op): array {
  $grants = [];
  if ($account->hasPermission('access private content')) {
    $grants['example'] = [1];
  }
  if ($account->id()) {
    $grants['example_author'] = [$account->id()];
  }
  return $grants;
}

/**
 * Type-specific version of hook_entity_grants().
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account object whose grants are requested.
 * @param string $op
 *   The entity operation to be performed, such as 'view', 'update', or 'delete'.
 *
 * @return array
 *   An array whose keys are "realms" of grants, and whose values are arrays of
 *   the grant IDs within this realm that this user is being granted.
 *
 * @ingroup entity_access
 */
function hook_ENTITY_TYPE_grants(AccountInterface $account, string $op): array {
  $grants = [];
  return $grants;
}

/**
 * Set permissions for a entity to be written to the database.
 *
 * When a entity is saved, a module implementing hook_entity_access_records() will
 * be asked if it is interested in the access permissions for a entity. If it is
 * interested, it must respond with an array of permissions arrays for that
 * entity.
 *
 * entity access grants apply regardless of the published or unpublished status
 * of the entity. Implementations must make sure not to grant access to
 * unpublished entitys if they don't want to change the standard access control
 * behavior. Your module may need to create a separate access realm to handle
 * access to unpublished entitys.
 *
 * Note that the grant values in the return value from your hook must be
 * integers and not boolean TRUE and FALSE.
 *
 * Each permissions item in the array is an array with the following elements:
 * - 'realm': The name of a realm that the module has defined in
 *   hook_entity_grants().
 * - 'gid': A 'grant ID' from hook_entity_grants().
 * - 'grant_view': If set to 1 a user that has been identified as a member
 *   of this gid within this realm can view this entity. This should usually be
 *   set to $entity->isPublished(). Failure to do so may expose unpublished content
 *   to some users.
 * - 'grant_update': If set to 1 a user that has been identified as a member
 *   of this gid within this realm can edit this entity.
 * - 'grant_delete': If set to 1 a user that has been identified as a member
 *   of this gid within this realm can delete this entity.
 * - langcode: (optional) The language code of a specific translation of the
 *   entity, if any. Modules may add this key to grant different access to
 *   different translations of a entity, such that (e.g.) a particular group is
 *   granted access to edit the Catalan version of the entity, but not the
 *   Hungarian version. If no value is provided, the langcode is set
 *   automatically from the $entity parameter and the entity's original language (if
 *   specified) is used as a fallback. Only specify multiple grant records with
 *   different languages for a entity if the site has those languages configured.
 *
 * A "deny all" grant may be used to deny all access to a particular entity or
 * entity translation:
 * @code
 * $records[] = [
 *   'realm' => 'all',
 *   'gid' => 0,
 *   'grant_view' => 0,
 *   'grant_update' => 0,
 *   'grant_delete' => 0,
 *   'langcode' => 'ca',
 * ];
 * @endcode
 * Note that another module entity access module could override this by granting
 * access to one or more entitys, since grants are additive. To enforce that
 * access is denied in a particular case, use hook_entity_access_records_alter().
 * Also note that a deny all is not written to the database; denies are
 * implicit.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity that has just been saved.
 *
 * @return array
 *   An array of access records as defined above.
 *
 * @see hook_entity_access_records_alter()
 * @ingroup entity_access
 */
function hook_entity_access_records(EntityInterface $entity): array {
  $records = [];
  // We only care about the entity if it has been marked private. If not, it is
  // treated just like any other entity and we completely ignore it.
  if ($entity->private->value) {
    // Only published Catalan translations of private entitys should be viewable
    // to all users. If we fail to check $entity->isPublished(), all users would be able
    // to view an unpublished entity.
    if ($entity->isPublished()) {
      $records[] = [
        'realm' => 'example',
        'gid' => 1,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
        'langcode' => 'ca',
      ];
    }
    // For the example_author array, the GID is equivalent to a UID, which
    // means there are many groups of just 1 user.
    // Note that an author can always view entitys they own, even if they have
    // status unpublished.
    if ($entity->getOwnerId()) {
      $records[] = [
        'realm' => 'example_author',
        'gid' => $entity->getOwnerId(),
        'grant_view' => 1,
        'grant_update' => 1,
        'grant_delete' => 1,
        'langcode' => 'ca',
      ];
    }
  }
  return $records;

}

/**
 * Type-specific version of hook_entity_access_records().
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity that has just been saved.
 *
 * @return array
 *   An array of grants as defined above.
 *
 * @ingroup entity_access
 */
function hook_ENTITY_TYPE_access_records(EntityInterface $entity): array {
  $records = [];
  return $records;
}

/**
 * Alter permissions for a entity before it is written to the database.
 *
 * Entity access modules establish rules for user access to content. entity access
 * records are stored in the {entity_access} table and define which permissions
 * are required to access a entity. This hook is invoked after entity access modules
 * returned their requirements via hook_entity_access_records(); doing so allows
 * modules to modify the $grants array by reference before it is stored, so
 * custom or advanced business logic can be applied.
 *
 * Upon viewing, editing or deleting a entity, hook_entity_grants() builds a
 * permissions array that is compared against the stored access records. The
 * user must have one or more matching permissions in order to complete the
 * requested operation.
 *
 * A module may deny all access to a entity by setting $grants to an empty array.
 *
 * The preferred use of this hook is in a module that bridges multiple entity
 * access modules with a configurable behavior, as shown in the example with the
 * 'is_preview' field.
 *
 * @param array $records
 *   The $records array returned by hook_entity_access_records().
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity for which the grants were acquired.
 *
 * @see hook_entity_access_records()
 * @see hook_entity_grants()
 * @see hook_entity_grants_alter()
 * @ingroup entity_access
 */
function hook_entity_access_records_alter(array &$records, EntityInterface $entity): void {
  // Our module allows editors to mark specific articles with the 'is_preview'
  // field. If the entity being saved has a TRUE value for that field, then only
  // our records are retained, and other records are removed. Doing so ensures
  // that our rules are enforced no matter what priority other records are given.
  if ($entity->is_preview) {
    // Our module records are set in $records['example'].
    $temp = $records['example'];
    // Now remove all module records but our own.
    $records = ['example' => $temp];
  }
}

/**
 * Type-specific version of hook_entity_access_records_alter().
 *
 * @param array $records
 *   The $grants array returned by hook_entity_access_records().
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity for which the grants were acquired.
 *
 * @ingroup entity_access
 */
function hook_ENTITY_TYPE_access_records_alter(array &$records, EntityInterface $entity): void {
}

/**
 * Alter user access rules when trying to view, edit or delete a entity.
 *
 * Entity access modules establish rules for user access to content.
 * hook_entity_grants() defines permissions for a user to view, edit or delete
 * entitys by building a $grants array that indicates the permissions assigned to
 * the user by each entity access module. This hook is called to allow modules to
 * modify the $grants array by reference, so the interaction of multiple entity
 * access modules can be altered or advanced business logic can be applied.
 *
 * The resulting grants are then checked against the records stored in the
 * {entity_access} table to determine if the operation may be completed.
 *
 * A module may deny all access to a user by setting $grants to an empty array.
 *
 * Developers may use this hook to either add additional grants to a user or to
 * remove existing grants. These rules are typically based on either the
 * permissions assigned to a user role, or specific attributes of a user
 * account.
 *
 * @param array $grants
 *   The $grants array returned by hook_entity_grants().
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account requesting access to content.
 * @param string $op
 *   The operation being performed, 'view', 'update' or 'delete'.
 *
 * @see hook_entity_grants()
 * @see hook_entity_access_records()
 * @see hook_entity_access_records_alter()
 * @ingroup entity_access
 */
function hook_entity_grants_alter(array &$grants, AccountInterface $account, string $op): void {
  // Our sample module never allows certain roles to edit or delete
  // content. Since some other entity access modules might allow this
  // permission, we expressly remove it by returning an empty $grants
  // array for roles specified in our variable setting.
  // Get our list of banned roles.
  $restricted = \Drupal::config('example.settings')->get('restricted_roles');

  if ($op != 'view' && !empty($restricted)) {
    // Now check the roles for this account against the restrictions.
    foreach ($account->getRoles() as $rid) {
      if (in_array($rid, $restricted)) {
        $grants = [];
      }
    }
  }
}

/**
 * Type-specific version of hook_entity_grants_alter().
 *
 * @param array $grants
 *   The $grants array returned by hook_entity_grants().
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account requesting access to content.
 * @param string $op
 *   The operation being performed, 'view', 'update' or 'delete'.
 *
 * @ingroup entity_access
 */
function hook_ENTITY_TYPE_grants_alter(array &$grants, AccountInterface $account, string $op): void {
}
