<?php

namespace Drupal\Tests\raft_entity_access\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\raft_entity_access_test\Entity\RocketShip;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests functionality when installing an entity type.
 *
 * @group raft_entity_access
 */
class RaftEntityAccessInstallTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'field',
    'user',
    'text',
    'raft_entity_access',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');

    $this->installConfig(self::$modules);
  }

  /**
   * Test installing an entity that uses access.
   *
   * Tests that access_records table is created when a module with a custom
   * entity that uses entity access is installed.
   */
  public function testEntityTypeInstall() {
    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $this->container->get('database');
    $schema = $connection->schema();
    self::assertFalse($schema->tableExists('rocket_ship_access_records'), 'Entity type access records table does not pre-exist.');
    $installer = $this->container->get('module_installer');
    $installer->install(['raft_entity_access_test']);
    self::assertTrue($schema->tableExists('rocket_ship_access_records'), 'Entity type access records table created.');
    $props = [
      'name' => 'tester',
    ];
    $perms = [
      'view own rocket_ship entities',
    ];
    $current_user = $this->setUpCurrentUser($props, $perms);
    $entity = RocketShip::create([
      'name' => 'Apollo 13',
      'origin' => 'Earth',
      'mass' => 367987,
      'status' => 1,
    ]);
    $errors = $entity->validate();
    self::assertCount(0, $errors);
    $entity->save();
    $count = $connection->select('rocket_ship_access_records')->countQuery()->execute()->fetchField();
    self::assertEquals(2, $count);
    $records = $connection->select('rocket_ship_access_records', 'r')->fields('r')->execute()->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($records as $record) {
      if ($record['realm'] === 'owner') {
        self::assertEquals($current_user->id(), $record['gid']);
      }
    }
  }

}
