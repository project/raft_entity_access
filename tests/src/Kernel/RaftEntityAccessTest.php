<?php

namespace Drupal\Tests\raft_entity_access\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\raft_entity_access_test\Entity\RocketShip;
use Drupal\raft_entity_access_test\Entity\RocketShipInterface;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\UserInterface;

/**
 * Tests basic access filtering on queries.
 *
 * @group raft_entity_access
 */
class RaftEntityAccessTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'field',
    'user',
    'text',
    'raft_entity_access',
    'raft_entity_access_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('rocket_ship');

    $this->installConfig(self::$modules);
  }

  /**
   * Tests some basic entity access.
   */
  public function testEntityAccess() {
    $props = [
      'name' => 'tester',
    ];
    $perms = [
      'view own rocket_ship entities',
    ];
    $current_user = $this->setUpCurrentUser($props, $perms);
    $entity = RocketShip::create([
      'name' => 'Apollo 13',
      'origin' => 'Earth',
      'mass' => 367987,
      'status' => 1,
    ]);
    $errors = $entity->validate();
    self::assertCount(0, $errors);
    $entity->save();
    self::assertEquals($current_user->id(), $entity->getOwnerId());
    self::assertAccess($entity, $current_user, TRUE, FALSE, FALSE);
    $perms = [
      'view published rocket_ship entities',
      'update any rocket_ship entities',
    ];
    $editor_user = $this->createUser($perms, 'editor');
    self::assertAccess($entity, $editor_user, TRUE, TRUE, FALSE);
    $perms = [
      'administer any rocket_ship entities',
      // Delete permission needs to be explicit.
      // @see \Drupal\raft_entity_access\EntityAccessRecordHandlerSql::access()
      'delete any rocket_ship entities',
    ];
    $admin_user = $this->createUser($perms, 'admin');
    self::assertAccess($entity, $admin_user, TRUE, TRUE, TRUE);
    $entity2 = RocketShip::create([
      'name' => 'Apollo 14',
      'origin' => 'Earth',
      'mass' => 367988,
      'status' => 0,
    ]);
    $entity2->save();
    self::assertAccess($entity2, $current_user, TRUE, FALSE, FALSE);
    self::assertAccess($entity2, $editor_user, FALSE, FALSE, FALSE);
    self::assertAccess($entity2, $admin_user, TRUE, TRUE, TRUE);
    // User who should have no access to any rocket_ship entities.
    $noaccess_user = $this->createUser(['access content'], 'no_access');
    self::assertAccess($entity, $noaccess_user, FALSE, FALSE, FALSE);
    self::assertAccess($entity2, $noaccess_user, FALSE, FALSE, FALSE);
    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $storage = $this->container->get('entity_type.manager')->getStorage('rocket_ship');
    $query = $storage->getQuery()->accessCheck(FALSE);
    $result = $query->execute();
    self::assertCount(2, $result);
    $tests = [
      [$current_user, 2],
      [$editor_user, 1],
      [$admin_user, 2],
      [$noaccess_user, 0],
    ];
    foreach ($tests as $row) {
      [$account, $expected_count] = $row;
      $this->setCurrentUser($account);
      $query = $storage->getQuery()->accessCheck();
      $result = $query->execute();
      self::assertCount($expected_count, $result, $account->label());
    }
  }

  /**
   * Assert access operations for a user on an account.
   *
   * @param \Drupal\raft_entity_access_test\Entity\RocketShipInterface $entity
   *   The entity.
   * @param \Drupal\user\UserInterface $account
   *   The user.
   * @param bool $view
   *   Expected view access.
   * @param bool $update
   *   Expected update access.
   * @param bool $delete
   *   Expected delete access.
   */
  protected static function assertAccess(RocketShipInterface $entity, UserInterface $account, bool $view, bool $update, bool $delete): void {
    foreach (['view' => $view, 'update' => $update, 'delete' => $delete] as $op => $expected) {
      $message = "Unexpected access result for operation $op for account " . $account->getDisplayName();
      self::assertSame($expected, $entity->access($op, $account), $message);
    }
  }

}
