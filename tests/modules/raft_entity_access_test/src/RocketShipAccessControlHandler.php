<?php

namespace Drupal\raft_entity_access_test;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\raft_entity_access\EntityAccessRecordHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Rocket ship entity.
 *
 * @see \Drupal\raft_entity_access_test\Entity\RocketShip.
 */
class RocketShipAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Access record storage.
   *
   * @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface
   */
  protected EntityAccessRecordHandlerInterface $accessRecordHandler;

  /**
   * Constructs a RocketShipAccessControlHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $access_record_handler
   *   The access record storage.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager, EntityAccessRecordHandlerInterface $access_record_handler) {
    parent::__construct($entity_type);
    $this->entityTypeManager = $entity_type_manager;
    $this->accessRecordHandler = $access_record_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    /** @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $accessRecordsHandler */
    $accessRecordsHandler = $entityTypeManager->getHandler($entity_type->id(), 'access_records');
    return new static(
      $entity_type,
      $entityTypeManager,
      $accessRecordsHandler
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\raft_entity_access_test\Entity\RocketShipInterface $entity */
    return $this->accessRecordHandler->access($entity, $operation, $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $permissions = ['create rocket ship entities'];
    $admin_permission = $this->entityType->getAdminPermission();
    if ($admin_permission) {
      $permissions[] = $admin_permission;
    }
    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
