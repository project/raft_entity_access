<?php

namespace Drupal\raft_entity_access_test;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Rocket ship entities.
 *
 * @ingroup raft_entity_access_test
 */
class RocketShipListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('Rocket ship ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\raft_entity_access_test\Entity\RocketShip $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.rocket_ship.edit_form',
      ['rocket_ship' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
