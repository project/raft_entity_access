<?php

namespace Drupal\raft_entity_access_test\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Rocket ship entities.
 *
 * @ingroup raft_entity_access_test
 */
interface RocketShipInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the Rocket ship name.
   *
   * @return string
   *   Name of the Rocket ship.
   */
  public function getName();

  /**
   * Sets the Rocket ship name.
   *
   * @param string $name
   *   The Rocket ship name.
   *
   * @return \Drupal\raft_entity_access_test\Entity\RocketShipInterface
   *   The called Rocket ship entity.
   */
  public function setName(string $name);

  /**
   * Gets the Rocket ship creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Rocket ship.
   */
  public function getCreatedTime();

  /**
   * Sets the Rocket ship creation timestamp.
   *
   * @param int $timestamp
   *   The Rocket ship creation timestamp.
   *
   * @return \Drupal\raft_entity_access_test\Entity\RocketShipInterface
   *   The called Rocket ship entity.
   */
  public function setCreatedTime($timestamp);

}
