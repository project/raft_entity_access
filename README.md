# RAFT Entity Access

Provides an entity access grants and records system similar to node_access, but for custom entity types.

Code based upon POC patch in [https://www.drupal.org/project/drupal/issues/777578#comment-11773055](https://www.drupal.org/project/drupal/issues/777578#comment-11773055).
One difference is that instead of modifying core EntityType and EntityTypeInterface, we have a trait
EntityTypeAccessTrait. Another difference is that in the default SQL implementation, each entity type
is given its own table named ENTITY_TYPE_access_records.

## Usage

When creating a custom entity type:
- The class annotation must include an access_records handler:
  - `"access_records" = "Drupal\raft_entity_access\EntityAccessRecordHandlerSql",`
- The access records come from an implementation of \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface
- In the entity type's access control handler, checkAccess() should call the access_records handler's access method
  to check grants, in the same way as `NodeAccessControlHandler::checkAccess()` calls `NodeGrantDatabaseStorage::access()`.

When updating a custom entity type to use entity access, you should notify
the definition update manager to make sure the table is created, ex:

```php
/**
 * Notify Drupal that the custom entity type has been updated.
 */
function raft_messaging_update_9001() {
  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();
  $entity_type = $definition_update_manager->getEntityType('message_builder');
  $definition_update_manager->updateEntityType($entity_type);
  return 'Updated the message_builder custom entity type.';
}
```

If needed as an alternative, you can execute a select query like the code
in `\Drupal\raft_entity_access\EventSubscriber\EntityTypeSubscriber::onCreate()`
to trigger creation of the table.

---
## TODO

- Make sure this does *not* apply to nodes.  (Handled in EntityTypeSubscriber but not in other code yet.)
- Add caching (see todos in patch code).
- Add command/function to apply/enable to existing entity types (?)  Should be handled when the type is updated.

## Questions

- Do we need to handle entity type delete?
- Drush command to rebuild records for an entity type?

---

- Create test module that makes a sample entity type that uses this.

- Check that JSONAPI and Views add the necessary tag via access_check(TRUE) and base table.
  - see Drupal\views\Plugin\views\query\Sql.php::execute()

- See NodeAccessControlHandler and EntityAccessControlHandler; our EntityAccessRecordHandlerSql is like the grantStorage used there.
  - NodeAccessControlHandler::access calls EntityAccessControlHandler::access which calls checkAccess() which in the node handler does the node_access query, but not in the entity handler.
  - (see https://www.drupal.org/project/drupal/issues/777578#comment-11782139)

`NodeAccessControlHandler::checkAccess()` calls `NodeGrantDatabaseStorage::access($node, $operation, $account)` which checks the grants.
Our entity's access control handler should similarly call `EntityAccessRecordHandlerSql::access()`.

---

- Should probably cache the list of entity types we care about.  (Currently determined in
`EntityAccessService::getAccessControlledTypes`)


