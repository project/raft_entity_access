<?php

namespace Drupal\raft_entity_access;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\raft_entity_access\Exception\EntityAccessRecordException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a default implementation for entity access record handlers.
 */
abstract class EntityAccessRecordHandlerBase implements EntityAccessRecordHandlerInterface {

  /**
   * Entity type ID for this handler.
   *
   * @var string
   */
  protected string $entityTypeId;

  /**
   * Information about the entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected EntityTypeInterface $entityType;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs an EntityAccessRecordHandlerBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeInterface $entity_type, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager) {
    $this->entityTypeId = $entity_type->id();
    $this->entityType = $entity_type;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * Finds out whether there are modules defining grants for this entity type.
   *
   * @return bool
   *   Whether there are modules providing grants for this entity type.
   */
  protected function hasGrantProviders(): bool {
    // We cannot know beforehand whether a generic grant provider will handle
    // this entity type. If there is any module providing generic entity grants,
    // we must assume the user may receive grants for this entity type.
    // @todo This is probably reasonable to cache.
    $generic = $this->moduleHandler->hasImplementations('entity_grants');
    return $generic || $this->moduleHandler->hasImplementations($this->entityTypeId . '_grants');
  }

  /**
   * {@inheritdoc}
   */
  public function acquireGrants(AccountInterface $account, $op): array {
    $grants = [];

    if ($this->hasGrantProviders()) {
      // Fetch grants using both the generic and specific hook.
      $generic = $this->moduleHandler->invokeAll('entity_grants', [$account, $op, $this->entityTypeId]);
      $specific = $this->moduleHandler->invokeAll($this->entityTypeId . '_grants', [$account, $op]);
      $grants = array_merge_recursive($generic, $specific);

      // Allow modules to alter the assigned grants.
      $context = ['account' => $account, 'op' => $op, 'entity_type_id' => $this->entityTypeId];
      $this->moduleHandler->alter('entity_grants', $grants, $context);
      $this->moduleHandler->alter($this->entityTypeId . '_grants', $grants, $context);
    }

    // Unlike node module, do not add a default 'all' grant. This should be
    // implemented by the specific entity module if desired. For example:
    // @code return array_merge(['all' => [0]], $grants);
    return $grants;
  }

  /**
   * {@inheritdoc}
   */
  public function acquireAccessRecords(ContentEntityInterface $entity): array {
    if ($entity->getEntityTypeId() !== $this->entityTypeId) {
      throw new EntityAccessRecordException('Trying to retrieve access records for mismatching entity type IDs.');
    }

    // Gather the access records from the generic hook implementation.
    $generic = $this->moduleHandler->invokeAll('entity_access_records', [$entity, $this->entityTypeId]);

    // Gather the access records from the specific hook implementation.
    $specific = $this->moduleHandler->invokeAll($this->entityTypeId . '_access_records', [$entity]);

    // Merge the result of both hooks into one array.
    $access_records = array_merge($generic, $specific);

    // Allow modules to alter the combined result.
    $this->moduleHandler->alter('entity_access_records', $access_records, $entity, $this->entityTypeId);
    $this->moduleHandler->alter($this->entityTypeId . '_access_records', $access_records, $entity);

    return $access_records;
  }

}
