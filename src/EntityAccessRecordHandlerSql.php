<?php

namespace Drupal\raft_entity_access;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseException;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an SQL implementation for entity access record handlers.
 */
class EntityAccessRecordHandlerSql extends EntityAccessRecordHandlerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The name of the database table for the access records.
   *
   * @var string
   */
  protected string $table;

  /**
   * Entity base table name.
   *
   * @var string|null
   */
  protected ?string $entityTable;

  /**
   * Entity id field name.
   *
   * @var string
   */
  protected string $idField;

  /**
   * Constructs an EntityAccessRecordHandlerSql object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(Connection $database, EntityTypeInterface $entity_type, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager) {
    parent::__construct($entity_type, $module_handler, $language_manager);
    $this->database = $database;
    $this->table = $this->entityTypeId . '_access_records';
    $this->entityTable = $entity_type->getBaseTable();
    $this->idField = $entity_type->getKey('id');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('database'),
      $entity_type,
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.CyclomaticComplexity)
   */
  public function writeAccessRecords(ContentEntityInterface $entity, array $access_records, ?string $realm = NULL, bool $delete = TRUE): void {
    if ($delete) {
      $query = $this->database->delete($this->table)->condition('id', $entity->id());
      if ($realm) {
        $query->condition('realm', [$realm, 'all'], 'IN');
      }
      $query->execute();
    }

    // Only perform work when entity access modules are active.
    if (!empty($access_records) && $this->hasGrantProviders()) {
      $query = $this->database->insert($this->table)->fields([
        'id',
        'langcode',
        'fallback',
        'realm',
        'gid',
        'grant_view',
        'grant_update',
        'grant_delete',
      ]);

      foreach ($access_records as $access_record) {
        // If a realm was given, skip access records that do not belong to it.
        if ($realm && $realm != $access_record['realm']) {
          continue;
        }

        // Make sure the entity ID is set correctly.
        $access_record['id'] = $entity->id();

        // If the access record defines a langcode, we only store the record in
        // that language. Otherwise, add an access record for every language the
        // entity is translated to.
        if (isset($access_record['langcode'])) {
          $grant_languages = [$access_record['langcode'] => $this->languageManager->getLanguage($access_record['langcode'])];
        }
        else {
          $grant_languages = $entity->getTranslationLanguages(TRUE);
        }

        $entity_langcode = $entity->language()->getId();
        foreach (array_keys($grant_languages) as $grant_langcode) {
          // Only write access records that actually grant something. Denies are
          // implicit so there's no point in storing those.
          if ($access_record['grant_view'] || $access_record['grant_update'] || $access_record['grant_delete']) {
            // The record with the original langcode is used as the fallback.
            $access_record['langcode'] = $grant_langcode;
            $access_record['fallback'] = (int) ($grant_langcode === $entity_langcode);
            $query->values($access_record);
          }
        }
      }

      $query->execute();
    }
  }

  /**
   * Get the stored access records for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The access records from the database.
   */
  public function getStoredAccessRecords(ContentEntityInterface $entity): array {
    $query = $this->database
      ->select($this->table, 'ea')
      ->fields('ea')
      ->condition('id', $entity->id());
    return $this->safeExecuteSelect($query)->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAccessRecords(): void {
    $this->database->truncate($this->table)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function countAccessRecords(): int {
    $query = $this->database->select($this->table)->countQuery();
    return $this->safeExecuteSelect($query)->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEntityAccessRecords(array $ids): void {
    $this->database->delete($this->table)
      ->condition('id', $ids, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function access(ContentEntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    $quick_access = $this->quickAccessChecks($entity, $operation, $account);
    if ($quick_access !== NULL) {
      return $quick_access;
    }

    // Check the database for potential access records.
    $query = $this->database->select($this->table);
    // Select '1' if we have any matching records.
    $query->addExpression('1');

    // Only interested in grants for the current operation.
    $query->condition('grant_' . $operation, 1, '>=');

    // Check for access records for this entity using the correct langcode.
    $id_condition = $query->andConditionGroup()
      ->condition('id', $entity->id())
      ->condition('langcode', $entity->language()->getId());

    // Check for the default access record which is saved with an ID of 0.
    $id_or_0_condition = $query->orConditionGroup()
      ->condition($id_condition)
      ->condition('id', 0);

    $query->condition($id_or_0_condition);
    $query->range(0, 1);

    // Add the user's grants to the query.
    $grants_condition = $this->buildGrantsQueryCondition($this->acquireGrants($account, $operation));
    if (count($grants_condition) > 0) {
      $query->condition($grants_condition);
      // @todo Introduce cache_context.user.entity_grants in core.services.yml and
      // use it here. @see NodeGrantDatabaseStorage::access().
      return $this->safeExecuteSelect($query)->fetchField() ? AccessResult::allowed() : AccessResult::neutral();
    }

    return AccessResult::neutral();
  }

  /**
   * Quick access checks that don't require an access records query.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to determine access.
   * @param string $operation
   *   The entity operation. One of 'view', 'update' or 'delete'.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for whom to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface|null
   *   The access result, or NULL if none of the quick checks is applicable.
   */
  protected function quickAccessChecks(ContentEntityInterface $entity, $operation, AccountInterface $account): ?AccessResultInterface {
    // Access records only support these operations.
    if (!in_array($operation, ['view', 'update', 'delete'])) {
      return AccessResult::neutral();
    }
    $entity_type = $entity->getEntityType();
    $admin_permission = $entity_type->getAdminPermission();
    // Since deletion is a kind of data loss, do not grant by default even to
    // a user with the admin permission.
    if ($admin_permission && ($operation !== 'delete') && $account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }
    // If no module implements the hooks or the entity does not have an id there
    // is no point in querying the database for access records.
    if (!$this->hasGrantProviders() || !$entity->id()) {
      // Return the equivalent of a default access record.
      return $operation === 'view' ? AccessResult::allowed() : AccessResult::neutral();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function alterQuery($query, array $tables, $operation, AccountInterface $account, $base_table): void {
    $langcode = $query->getMetaData('langcode');
    if (!$langcode) {
      $langcode = FALSE;
    }

    // Get the query conditions for the user's grants.
    $grant_conditions = $this->buildGrantsQueryCondition($this->acquireGrants($account, $operation));
    if (!count($grant_conditions)) {
      $query->alwaysFalse();
      return;
    }

    // Find all instances of the base table being joined -- could appear
    // more than once in the query, and could be aliased. Join each one to
    // the entity access table.
    foreach ($tables as $alias => $table_info) {
      $table = $table_info['table'];

      if (!($table instanceof SelectInterface) && $table === $base_table) {
        // Create the subquery that will look for access records.
        $subquery = $this->database->select($this->table, 'ea')->fields('ea', ['id']);

        // Add the operation to the query.
        $subquery->condition("ea.grant_$operation", 1, '>=');

        // If the user has any grants, add them to the query.
        if (count($grant_conditions)) {
          $subquery->condition($grant_conditions);
        }

        // Add language-based filtering if this is a multilingual site.
        if ($this->languageManager->isMultilingual()) {
          // If no specific langcode to check for is given, use the fallback.
          if (!$langcode) {
            $subquery->condition('ea.fallback', 1);
          }
          // If a specific langcode is given, use the access record for it.
          else {
            $subquery->condition('ea.langcode', $langcode);
          }
        }

        // Join the entity's ID field to the one in the access records.
        $field = $this->entityType->getKey('id');
        $subquery->where("$alias.$field = ea.id");

        // Use an EXISTS condition with the subquery.
        $query->exists($subquery);
      }
    }
  }

  /**
   * Creates a query condition from an array of grants.
   *
   * @param array $grants
   *   An array of grants, as returned by ::acquireGrants().
   *
   * @return \Drupal\Core\Database\Query\Condition
   *   A condition object to be passed to $query->condition().
   *
   * @see \Drupal\Core\Entity\EntityAccessRecordHandlerInterface::acquireGrants()
   */
  protected function buildGrantsQueryCondition(array $grants): Condition {
    $condition = $this->database->condition('OR');

    foreach ($grants as $realm => $gids) {
      if (!empty($gids)) {
        $and = $this->database->condition('AND');
        $condition->condition($and
          ->condition('gid', $gids, 'IN')
          ->condition('realm', $realm)
        );
      }
    }

    return $condition;
  }

  /**
   * Executes a select query while making sure the database table exists.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The select object to be executed.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   A prepared statement, or NULL if the query is not valid.
   *
   * @throws \Exception
   *   Thrown if the table could not be created or the database connection
   *   failed.
   */
  protected function safeExecuteSelect(SelectInterface $query): ?StatementInterface {
    try {
      return $query->execute();
    }
    catch (\Exception $e) {
      // If there was an exception, try to create the table.
      if ($this->ensureTableExists()) {
        return $query->execute();
      }
      // Some other failure that we can not recover from.
      throw new PluginException($e->getMessage(), 0, $e);
    }
  }

  /**
   * Checks if the tree table exists and create it if not.
   *
   * @return bool
   *   TRUE if the table was created, FALSE otherwise.
   */
  protected function ensureTableExists(): bool {
    try {
      $this->database->schema()->createTable($this->table, $this->schemaDefinition());
    }
    catch (DatabaseException $e) {
      // If another process has already created the table, attempting to
      // recreate it will throw an exception. In this case just catch the
      // exception and do nothing.
    }
    catch (\Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Defines the schema to store access records.
   *
   * @return array
   *   Schema API definition.
   */
  protected function schemaDefinition(): array {
    return [
      'description' => 'Identifies which realm/grant pairs a user must possess in order to view, update, or delete specific entities.',
      'fields' => [
        'id' => [
          'description' => 'The entity id this record affects.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'langcode' => [
          'description' => 'The {language}.langcode of this entity.',
          'type' => 'varchar_ascii',
          'length' => 12,
          'not null' => TRUE,
          'default' => '',
        ],
        'fallback' => [
          'description' => 'Boolean indicating whether this record should be used as a fallback if a language condition is not provided.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 1,
          'size' => 'tiny',
        ],
        'gid' => [
          'description' => "The grant ID a user must possess in the specified realm to gain this row's privileges on the entity.",
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'realm' => [
          'description' => 'The realm in which the user must possess the grant ID. Modules can define one or more realms by implementing hook_entity_grants().',
          'type' => 'varchar_ascii',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'grant_view' => [
          'description' => 'Boolean indicating whether a user with the realm/grant pair can view this entity.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
        ],
        'grant_update' => [
          'description' => 'Boolean indicating whether a user with the realm/grant pair can edit this entity.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
        ],
        'grant_delete' => [
          'description' => 'Boolean indicating whether a user with the realm/grant pair can delete this entity.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
        ],
      ],
      'primary key' => ['id', 'gid', 'realm', 'langcode'],
      'foreign keys' => [
        'affected_entity' => [
          'table' => $this->entityTable,
          'columns' => ['id' => $this->idField],
        ],
      ],
    ];

  }

}
