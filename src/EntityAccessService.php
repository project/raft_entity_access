<?php

namespace Drupal\raft_entity_access;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service for handling entity access.
 */
class EntityAccessService {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RequestStack $requestStack, Renderer $renderer, ModuleHandlerInterface $moduleHandler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Gets a list of the entity types that use raft_entity_access.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   Array of type definitions.
   */
  public function getAccessControlledTypes(): array {
    static $types;
    if (!isset($types)) {
      $types = [];
      $type_definitions = $this->entityTypeManager->getDefinitions();
      foreach ($type_definitions as $type) {
        if ($type->hasHandlerClass('access_records')) {
          $types[$type->id()] = $type;
        }
      }
    }
    return $types;
  }

  /**
   * Determine whether a type is access controlled.
   *
   * @param string $type_name
   *   The name of the entity type.
   *
   * @return bool
   *   TRUE if access controlled.
   */
  public function isAccessControlledType(string $type_name): bool {
    $types = $this->getAccessControlledTypes();
    return isset($types[$type_name]);
  }

  /**
   * Alter a query if necessary.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function queryAlter(AlterableInterface $query): void {
    $types = $this->getAccessControlledTypes();
    if (empty($types)) {
      return;
    }
    foreach (array_keys($types) as $type_id) {
      $tag = $type_id . '_access';
      if ($query->hasTag($tag)) {
        $this->alterQueryForType($query, $type_id);
        break;
      }
    }
  }

  /**
   * Alter a query to check access for a specified entity type definition.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   Query.
   * @param string $type_id
   *   Entity type id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   */
  protected function alterQueryForType(AlterableInterface $query, string $type_id): void {
    // Read meta-data from query, if provided.
    $account = $query->getMetaData('account') ?? $this->currentUser;
    $op = $query->getMetaData('op') ?? 'view';

    $entity_type = $this->entityTypeManager->getDefinition($type_id);
    $admin_permission = $entity_type->getAdminPermission();
    // If $account has the admin perm we let them bypass entity access as well.
    if ($admin_permission && $account->hasPermission($admin_permission)) {
      return;
    }

    $tables = $query->getTables();
    $base_table = $this->getJoinTableForType($query, $type_id);

    /** @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $handler */
    $handler = $this->entityTypeManager->getHandler($type_id, 'access_records');
    $handler->alterQuery($query, $tables, $op, $account, $base_table);

    // Bubble the 'user.ENTITY_TYPE_grants:$op' cache context to the current render
    // context.
    $request = $this->requestStack->getCurrentRequest();
    if ($request->isMethodCacheable() && $this->renderer->hasRenderContext()) {
      // @todo RAFT-4963 actually define this cache context.
      // $build = ['#cache' => ['contexts' => ['user.' . $type_id . '_grants:' . $op]]];
      $build = ['#cache' => ['contexts' => ['user']]];
      $this->renderer->render($build);
    }
  }

  /**
   * Find a table in the query to join to.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   Query.
   * @param string $type_id
   *   Entity type id.
   *
   * @return string|null
   *   The table name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getJoinTableForType(AlterableInterface $query, string $type_id): ?string {
    $tables = $query->getTables();
    $base_table = $query->getMetaData('base_table');

    // If the base table is not given, default to one of the entity base tables.
    if (!$base_table) {
      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $this->entityTypeManager->getStorage($type_id)->getTableMapping();
      $entity_tables = $table_mapping->getTableNames();

      foreach ($tables as $table_info) {
        // Ignore subqueries.
        if ($table_info instanceof SelectInterface) {
          continue;
        }
        $table = $table_info['table'];
        // Prefer entity and entity_type_data over other tables.
        if ($table == $table_mapping->getBaseTable() || $table === $table_mapping->getDataTable()) {
          $base_table = $table;
          break;
        }
        // If the table is one of the entity tables, join against it.
        if (in_array($table, $entity_tables)) {
          $base_table = $table;
        }
      }

      // Bail out if we still don't have a table to join to.
      if (!$base_table) {
        throw new \Exception('Query tagged for entity access but there is no entity table, specify the base_table using meta data.');
      }
    }
    return $base_table;
  }

  /**
   * Get the stored access records for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The access records from the database.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getStoredAccessRecords(EntityInterface $entity): array {
    /** @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $handler */
    $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'access_records');
    return $handler->getStoredAccessRecords($entity);
  }

  /**
   * Handle entity insert or update.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function handleEntitySave(EntityInterface $entity): void {
    if (!($entity instanceof ContentEntityInterface)) {
      return;
    }
    if ($entity->isDefaultRevision() && $this->isAccessControlledType($entity->getEntityTypeId())) {
      /** @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $handler */
      $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'access_records');
      $records = $handler->acquireAccessRecords($entity);
      $handler->writeAccessRecords($entity, $records);
    }
  }

  /**
   * Handle entity delete.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function handleEntityDelete(EntityInterface $entity): void {
    if (!($entity instanceof ContentEntityInterface)) {
      return;
    }
    if ($entity->isDefaultRevision() && $this->isAccessControlledType($entity->getEntityTypeId())) {
      /** @var \Drupal\raft_entity_access\EntityAccessRecordHandlerInterface $handler */
      $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'access_records');
      $handler->deleteEntityAccessRecords([$entity->id()]);
    }
  }

}
