<?php

namespace Drupal\raft_entity_access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an interface for entity access record handlers.
 *
 * @ingroup entity_access
 */
interface EntityAccessRecordHandlerInterface extends EntityHandlerInterface {

  /**
   * Retrieves the entity access grants for a user.
   *
   * Note that, regardless of the grants gathered through hook_entity_grants()
   * and hook_ENTITY_TYPE_grants(), a default "all" grant with entity ID 0 will
   * always be returned.
   *
   * The list of grants can be altered using hook_entity_grants_alter() and
   * hook_ENTITY_TYPE_grants_alter().
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account object for the user performing the operation.
   * @param string $op
   *   The operation that the user is trying to perform.
   *
   * @return array
   *   An associative array in which the keys are realms and the values are the
   *   grant IDs the user has access to for those realms.
   */
  public function acquireGrants(AccountInterface $account, $op): array;

  /**
   * Gets the list of entity access records for an entity.
   *
   * This function is called to retrieve the access records for an entity. It
   * collects them from hook_entity_access_records() and
   * hook_ENTITY_TYPE_access_records() implementations.
   *
   * After both hooks have been invoked, other modules may choose to alter the
   * gathered access records using either of the following hooks:
   * - hook_entity_access_records_alter()
   * - hook_ENTITY_TYPE_access_records_alter()
   *
   * Note: this is the counterpart to the confusingly-named acquireGrants in the
   * node access system.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to acquire access records for.
   *
   * @return array
   *   The access records for the entity.
   *
   * @see \Drupal\node\NodeAccessControlHandler::acquireGrants()
   */
  public function acquireAccessRecords(ContentEntityInterface $entity): array;

  /**
   * Writes a list of access records to the database, deleting existing ones.
   *
   * If a realm is provided, it will only delete access records from that realm,
   * but it will always delete an access record from the 'all' realm. Modules
   * that use the entity access record system can use this method when doing
   * mass updates due to widespread permission changes.
   *
   * Note: Don't call this method directly from a contributed module. Call
   * \Drupal\node\NodeAccessControlHandlerInterface::acquireGrants() instead. @todo
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which access records are being written.
   * @param array $access_records
   *   A list of access records to write. Each record is an array that must
   *   contain the following keys:
   *   - realm: Name of the realm
   *   - gid: ID within the realm
   *   - grant_view: Whether to grant view access (0 or 1)
   *   - grant_update: Whether to grant update access (0 or 1)
   *   - grant_delete: Whether to grant delete access (0 or 1)
   * @param string|null $realm
   *   (optional) Read, write and delete access records for this realm only.
   *   Defaults to NULL.
   * @param bool $delete
   *   (optional) If FALSE, does not delete records. This is only for
   *   optimization purposes and assumes the caller has already performed a mass
   *   delete of some form. Defaults to TRUE.
   */
  public function writeAccessRecords(ContentEntityInterface $entity, array $access_records, string $realm = NULL, bool $delete = TRUE): void;

  /**
   * Deletes all access records.
   */
  public function deleteAccessRecords(): void;

  /**
   * Counts available access records.
   *
   * @return int
   *   Returns the amount of access records.
   */
  public function countAccessRecords(): int;

  /**
   * Remove the access records belonging to certain entities.
   *
   * @param array $ids
   *   A list of entity IDs. The access records belonging to these entities will
   *   be deleted.
   */
  public function deleteEntityAccessRecords(array $ids): void;

  /**
   * Determines access to an entity based on access records.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to determine access.
   * @param string $operation
   *   The entity operation. One of 'view', 'update' or 'delete'.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for whom to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result, either allowed or neutral. If there are no access
   *   records, the default access record defined by writeDefaultAccessRecord()
   *   is applied.
   *
   * @see hook_entity_grants()
   * @see hook_entity_access_records()
   * @see \Drupal\Core\EntityAccessRecordHandlerInterface::writeDefaultAccessRecord()
   */
  public function access(ContentEntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface;

  /**
   * Alters a query when entity access is required.
   *
   * @param mixed $query
   *   Query that is being altered.
   * @param array $tables
   *   A list of tables that need to be part of the alter.
   * @param string $operation
   *   The operation to be performed on the entity. Possible values are:
   *    - "view"
   *    - "update"
   *    - "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   A user object representing the user for whom the operation is to be
   *   performed.
   * @param string $base_table
   *   The base table of the query.
   */
  public function alterQuery($query, array $tables, string $operation, AccountInterface $account, string $base_table): void;

}
