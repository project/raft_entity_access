<?php

namespace Drupal\raft_entity_access\EventSubscriber;

use Drupal\Core\Entity\EntityTypeEvent;
use Drupal\Core\Entity\EntityTypeEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber for entity type events.
 */
class EntityTypeSubscriber implements EventSubscriberInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[EntityTypeEvents::CREATE][] = ['onCreate'];
    $events[EntityTypeEvents::UPDATE][] = ['onCreate'];
    return $events;
  }

  /**
   * Handle creation of a new entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeEvent $event
   *   The event.
   */
  public function onCreate(EntityTypeEvent $event): void {
    $entity_type = $event->getEntityType();

    if ($entity_type->hasHandlerClass('access_records')) {
      $handler = $this->entityTypeManager->getHandler($entity_type->id(), 'access_records');
      // Trigger a query that will check that the table exists and create it if necessary.
      // Doing this instead of just making ensureTableExists public and calling it because it should
      // be faster when the table does exist, and it usually will.
      // See also https://www.drupal.org/project/drupal/issues/2371709.
      $handler->countAccessRecords();
    }

  }

}
