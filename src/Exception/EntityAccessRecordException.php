<?php

namespace Drupal\raft_entity_access\Exception;

/**
 * Defines an exception thrown when access record handling fails.
 */
class EntityAccessRecordException extends \Exception {}
